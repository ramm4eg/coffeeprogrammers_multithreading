
package Simulation;

import java.util.Date;

import CoffeeSystem.CoffeeSystem;
import Programmers.Programmer;


/**
 * This class is the main class of the exercise. It contains the main method, which instantiates the
 * coffee system and all the programmers that want coffee.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class Starter {

  /**
   * Main method for the people, who don't like the SWT GUI and don't need any runtime
   * parameterization.
   * 
   * @param args arguments of this method are ignored
   */
  public static void main(String[] args) {
    System.out.println(simulate());
  }

  /**
   * Instantiate the coffee system and all the programmers that want coffee. Gather the statistics
   * after the simulation and format it into a result text.
   * 
   * @return statistics of the simulation as text
   */
  public static String simulate() {
    CoffeeSystem coffeeSystem = new CoffeeSystem();

    int numberOfProgrammers = SimulationConfig.numberOfProgrammers;
    Programmer[] programmers = new Programmer[numberOfProgrammers];

    Date globalStart = new Date();

    for (int i = 0; i < numberOfProgrammers; i++) {
      Programmer programmer = new Programmer();
      programmers[i] = programmer;
      programmer.acquireCoffee(coffeeSystem);
    }

    for (int i = numberOfProgrammers - 1; i >= 0; i--) {
      if (programmers[i].isAlive()) {
        try {
          programmers[i].join();
        } catch (InterruptedException e) {
        }
      }
    }

    Date globalEnd = new Date();
    long globalTime = globalEnd.getTime() - globalStart.getTime();

    // print results
    String results = "";

    int coffeeSoldForCredit = coffeeSystem.getCoffeeSoldForCredit();
    int coffeeSoldForCash = coffeeSystem.getCoffeeSoldForCash();
    results += "=================================================\n";
    results += "Coffee sold:\n";
    results += "    for credit: " + coffeeSoldForCredit + "\n";
    results += "    for cash: " + coffeeSoldForCash + "\n";
    results += "            total: " + (coffeeSoldForCash + coffeeSoldForCredit) + "\n";

    results += "\n";
    results += "Coffee dispensed:" + "\n";
    int[][] coffeeDispensed = coffeeSystem.getDispensedCoffee();
    for (int i = 0; i < coffeeDispensed.length; i++) {
      results += "    MACHINE " + i + ":" + "\n";
      results += "        Espresso: " + coffeeDispensed[i][0] + "\n";
      results += "        Latte Macchiato: " + coffeeDispensed[i][1] + "\n";
      results += "        Cappuccino: " + coffeeDispensed[i][2] + "\n";
      results +=
          "            total: "
              + (coffeeDispensed[i][0] + coffeeDispensed[i][1] + coffeeDispensed[i][2]) + "\n";
    }

    results += "\n";
    results += "Time getting coffee including waiting in the queue:" + "\n";
    long fastest = getFastestTime(programmers);
    results += "    fastest: " + fastest + " ms" + "\n";
    long slowest = getSlowestTime(programmers);
    results += "    slowest: " + slowest + " ms" + "\n";
    long avarage = getAvarageTime(programmers);
    results += "    avarage: " + avarage + " ms" + "\n";

    results += "\n";
    results += "Overall simulation time: " + globalTime + " ms" + "\n";

    return results;
  }


  /**
   * Gather the coffee getting time from all programmers and calculate average out of it.
   * 
   * @param programmers all programmers
   * @return average coffee getting time
   */
  private static long getAvarageTime(Programmer[] programmers) {
    long timeTotal = 0;
    for (Programmer programmer : programmers) {
      timeTotal += programmer.getUsedTime();
    }
    return timeTotal / programmers.length;
  }

  /**
   * Gather the coffee getting time from all programmers and calculate slowest of them.
   * 
   * @param programmers all programmers
   * @return slowest coffee getting time
   */
  private static long getSlowestTime(Programmer[] programmers) {
    long max = programmers[0].getUsedTime();
    for (int i = 1; i < programmers.length; i++) {
      long time = programmers[i].getUsedTime();
      if (time > max) {
        max = time;
      }
    }
    return max;
  }

  /**
   * Gather the coffee getting time from all programmers and calculate fastest of them.
   * 
   * @param programmers all programmers
   * @return fastest coffee getting time
   */
  private static long getFastestTime(Programmer[] programmers) {
    long min = programmers[0].getUsedTime();
    for (int i = 1; i < programmers.length; i++) {
      long time = programmers[i].getUsedTime();
      if (time < min) {
        min = time;
      }
    }
    return min;
  }

  /**
   * Change simulation parameterization.
   * 
   * @param numOfProgrammers number of programmers as string
   * @param numOfSelectionSlots number of coffee kind selection slots as string
   * @param numOfPaymentSlots number of payment slots as string
   * @param numOfCoffeeMachines number of coffee machines as string
   */
  public static void prepareConfig(String numOfProgrammers, String numOfSelectionSlots,
      String numOfPaymentSlots, String numOfCoffeeMachines) {
    SimulationConfig.setNumberOfProgrammers(numOfProgrammers);
    SimulationConfig.setNumberOfSelectionSlots(numOfSelectionSlots);
    SimulationConfig.setNumberOfPaymentSlots(numOfPaymentSlots);
    SimulationConfig.setNumberOfCoffeeMachines(numOfCoffeeMachines);
  }
}
