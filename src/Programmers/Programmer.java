
package Programmers;

import java.util.Date;

import CoffeeSystem.CashRegister;
import CoffeeSystem.CoffeeKind;
import CoffeeSystem.CoffeeMachine;
import CoffeeSystem.CoffeeSystem;
import CoffeeSystem.PaymentKind;
import Simulation.SimulationConfig;

/**
 * This class represents a programmer that wants some coffee. Every programmer runs in his own
 * thread, so they can want their coffee simultaneously.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class Programmer extends Thread {

  private CoffeeSystem coffeeSystem;
  private long usedTime = 0;

  /**
   * Get in the queue of programmers that want coffee at the provided coffee system.
   * 
   * @param coffeeSystem system to get coffee from
   */
  public void acquireCoffee(CoffeeSystem coffeeSystem) {
    this.coffeeSystem = coffeeSystem;
    start();
  }

  /**
   * {@inheritDoc}
   * 
   * @see java.lang.Thread#run()
   */
  public void run() {
    if (coffeeSystem == null) {
      System.out.println("Can't get any coffee - no coffee system in sight.");
      return;
    }

    Date start = new Date(); // start measuring time

    CoffeeKind selection = selectCoffeeKind();
    payForCoffee(selection);
    getCoffee(selection);

    // end measuring time
    Date end = new Date();
    usedTime = end.getTime() - start.getTime();
  }

  /**
   * Get selected coffee from a free coffee machine. Might need to wait for one.
   * 
   * @param selection selected coffee kind
   */
  private void getCoffee(CoffeeKind selection) {
    CoffeeMachine coffeeMachine = coffeeSystem.getCoffeeMachine();

    findACup();
    putItUnderTheOutlet();
    pickTheTypeOfCoffee(selection, coffeeMachine); // and wait for coffee machine to be done
    takeTheCupAndLeave();

    coffeeSystem.freeUpCoffeeMachine(coffeeMachine);
  }

  /**
   * Take the cup and leave the simulation. This takes some time.
   */
  private void takeTheCupAndLeave() {
    try {
      sleep(SimulationConfig.LEAVING_TIME);
    } catch (InterruptedException e) {
    }
  }

  /**
   * Pick the type of coffee that was payed for. This takes some time. Afterwards the coffee machine
   * will dispense that coffee, that also takes time.
   */
  private void pickTheTypeOfCoffee(CoffeeKind selection, CoffeeMachine coffeeMachine) {
    try {
      sleep(SimulationConfig.COFFEE_TYPE_PICKING_TIME);
    } catch (InterruptedException e) {
    }
    coffeeMachine.pourCoffee(selection);
  }

  /**
   * Put the cup under the outlet. This takes some time.
   */
  private void putItUnderTheOutlet() {
    try {
      sleep(SimulationConfig.CUP_UNDER_OUTLET_PUTTING_TIME);
    } catch (InterruptedException e) {
    }
  }

  /**
   * Find a relatively clean coffee cup. This takes some time.
   */
  private void findACup() {
    try {
      sleep(SimulationConfig.CUP_FINDING_TIME);
    } catch (InterruptedException e) {
    }
  }

  /**
   * Read the menu and select the favorite type of coffee. Might need to wait, if there are too many
   * people in front of the menu.
   * 
   * @return selected coffee type
   */
  private CoffeeKind selectCoffeeKind() {
    coffeeSystem.getSelectionSlot();

    CoffeeKind[] availableCoffeeKinds = coffeeSystem.getAvailableCoffeeKinds();
    CoffeeKind selection = chooseCoffeeKind(availableCoffeeKinds);

    coffeeSystem.freeUpSelectionSlot();
    return selection;
  }

  /**
   * Choose the favorite (well, random) coffee kind from the offered ones.
   * 
   * @param availableCoffeeKinds offered kinds of coffee
   * @return favorite coffee kind
   */
  private CoffeeKind chooseCoffeeKind(CoffeeKind[] availableCoffeeKinds) {
    try {
      sleep(SimulationConfig.COFFEE_CHOOSING_TIME);
    } catch (InterruptedException e) {
    }

    int selectionIndex = (int) (Math.random() * (availableCoffeeKinds.length));
    return availableCoffeeKinds[selectionIndex];
  }

  /**
   * Pay for the selected coffee. Might need to wait for a free cash register.
   * 
   * @param selection selected coffee kind
   */
  private void payForCoffee(CoffeeKind selection) {
    CashRegister cashRegister = coffeeSystem.getCashRegister();

    PaymentKind[] availablePaymentKinds = coffeeSystem.getAvailablePaymentKinds();
    PaymentKind paymentKind = choosePaymentKind(availablePaymentKinds);

    cashRegister.takePayment(paymentKind, selection);
    coffeeSystem.freeUpRegisterSlot(cashRegister);
  }

  /**
   * Choose the favorite (well, also random) payment method from the provided ones
   * 
   * @param availablePaymentKinds all accepted payment kinds
   * 
   * @return favorite payment kind
   */
  private PaymentKind choosePaymentKind(PaymentKind[] availablePaymentKinds) {
    int paymentKindIndex = (int) (Math.random() * (availablePaymentKinds.length));
    PaymentKind paymentKind = availablePaymentKinds[paymentKindIndex];
    return paymentKind;
  }

  /**
   * @return time used to get coffee including the time spent waiting in all queues
   */
  public long getUsedTime() {
    return usedTime;
  }

}
