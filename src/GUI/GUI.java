
package GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import Simulation.SimulationConfig;
import Simulation.Starter;

/**
 * This class implements a simplistic GUI for the coffee system simulation
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class GUI {

  private static final String SIMULATE_BUTTON_TEXT = "Simulate";
  private static final String NUMBER_OF_COFFEE_MACHINES_TEXT = "Number of coffee machines: ";
  private static final String CASH_REGISTERS_TEXT =
      "How many people can pay for coffee simultaneously? ";
  private static final String SELECTION_SLOTS_TEXT =
      "How many people can choose coffee simultaneously? ";
  private static final String NUMBER_OF_PROGRAMMERS_TEXT = "Number of programmers: ";
  private static final String WINDOW_TITLE = "Coffee Powered Programmers";
  private static final String SIMULATION_RUNNING_TEXT = "Simulation is running, please wait";

  /**
   * This class implements a verifier for a text field, which only allows typing of integer numbers
   */
  public class NumbersVerifier implements VerifyListener {

    /**
     * {@inheritDoc}
     * 
     * @see org.eclipse.swt.events.VerifyListener#verifyText(org.eclipse.swt.events.VerifyEvent)
     */
    @Override
    public void verifyText(VerifyEvent e) {
      Text text = (Text) e.getSource();

      // try creating a new text and check if it's an integer
      String oldString = text.getText();
      String newString = oldString.substring(0, e.start) + e.text + oldString.substring(e.end);
      try {
        Integer.parseInt(newString);
      } catch (NumberFormatException ex) {
        e.doit = false;
      }
    }

  }

  private Text results;
  private Shell shell;
  private Text programmersText;
  private Text selectionSlotsText;
  private Text payingSlotsText;
  private Text machinesText;

  /**
   * This class implements a button listener for the "Simulate" button. It starts the coffee system
   * simulation and displays its results afterwards.
   */
  public class StartSimulation implements SelectionListener {

    private int numberOfPoints = 0;

    /**
     * {@inheritDoc}
     * 
     * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
     */
    @Override
    public void widgetDefaultSelected(SelectionEvent arg0) {
      // do nothing
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
     */
    @Override
    public void widgetSelected(SelectionEvent arg0) {
      Starter.prepareConfig(programmersText.getText(), selectionSlotsText.getText(),
          payingSlotsText.getText(), machinesText.getText());

      SimulationThread simulation = new SimulationThread();
      simulation.start();

      try {
        while (simulation.isAlive()) {
          updateResultScreen();
          Thread.sleep(500);
        }
        simulation.join();
      } catch (InterruptedException e) {
      }

      String resultString = simulation.getResult();
      System.out.println(resultString);
      results.setText(resultString);
      shell.layout();
      shell.pack();
    }

    /**
     * Write temporary text to the simulation result section of the GUI
     */
    private void updateResultScreen() {
      String resultText = SIMULATION_RUNNING_TEXT;
      for (int i = 0; i < numberOfPoints; i++) {
        resultText += ".";
      }

      numberOfPoints = (numberOfPoints + 1) % 4;

      results.setText(resultText);
      shell.layout();
      shell.pack();
      shell.redraw();
      shell.update();
    }
  }

  /**
   * Build up the GUI and display it to the user
   */
  public void open() {
    Display display = Display.getDefault();
    shell = new Shell(display);
    shell.setText(WINDOW_TITLE);
    shell.setSize(500, 500);

    shell.setLayout(new GridLayout(3, true));

    NumbersVerifier numbersVerificator = new NumbersVerifier();

    Label programmersLabel = new Label(shell, 0);
    programmersLabel.setLayoutData(new GridData(0, 0, true, false, 2, 1));
    programmersLabel.setText(NUMBER_OF_PROGRAMMERS_TEXT);
    programmersText = new Text(shell, SWT.BORDER | SWT.RIGHT);
    GridData textGd = new GridData(SWT.RIGHT, 0, false, false, 1, 1);
    textGd.widthHint = 100;
    programmersText.setLayoutData(textGd);
    programmersText.addVerifyListener(numbersVerificator);
    programmersText.setText("" + SimulationConfig.numberOfProgrammers);

    Label selectionSlotsLabel = new Label(shell, 0);
    selectionSlotsLabel.setLayoutData(new GridData(0, 0, true, false, 2, 1));
    selectionSlotsLabel.setText(SELECTION_SLOTS_TEXT);
    selectionSlotsText = new Text(shell, SWT.BORDER | SWT.RIGHT);
    selectionSlotsText.setLayoutData(textGd);
    selectionSlotsText.addVerifyListener(numbersVerificator);
    selectionSlotsText.setText("" + SimulationConfig.numberOfSelectionSlots);

    Label payingSlotsLabel = new Label(shell, 0);
    payingSlotsLabel.setLayoutData(new GridData(0, 0, true, false, 2, 1));
    payingSlotsLabel.setText(CASH_REGISTERS_TEXT);
    payingSlotsText = new Text(shell, SWT.BORDER | SWT.RIGHT);
    payingSlotsText.setLayoutData(textGd);
    payingSlotsText.addVerifyListener(numbersVerificator);
    payingSlotsText.setText("" + SimulationConfig.numberOfPaymentSlots);

    Label machinesLabel = new Label(shell, 0);
    machinesLabel.setLayoutData(new GridData(0, 0, true, false, 2, 1));
    machinesLabel.setText(NUMBER_OF_COFFEE_MACHINES_TEXT);
    machinesText = new Text(shell, SWT.BORDER | SWT.RIGHT);
    machinesText.setLayoutData(textGd);
    machinesText.addVerifyListener(numbersVerificator);
    machinesText.setText("" + SimulationConfig.numberOfCoffeeMachines);

    results = new Text(shell, SWT.V_SCROLL | SWT.BORDER | SWT.END | SWT.MULTI | SWT.LEFT);
    GridData resultsGd = new GridData(SWT.FILL, 0, true, true, 3, 1);
    resultsGd.heightHint = 400;
    results.setLayoutData(resultsGd);


    Label empty = new Label(shell, 0);
    empty.setLayoutData(new GridData(0, 0, true, false, 2, 1));

    Button button = new Button(shell, 0);
    button.setText(SIMULATE_BUTTON_TEXT);

    SelectionListener buttonClick = new StartSimulation();
    button.addSelectionListener(buttonClick);

    shell.pack();
    shell.open();

    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }

}
