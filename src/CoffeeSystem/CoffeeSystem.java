
package CoffeeSystem;

import Simulation.SimulationConfig;

/**
 * This class represents a coffee system, that contains multiple money slots, multiple coffee
 * machines and can serve multiple customers simultaneously.
 * 
 * It is an API for customers, that want to buy coffee.
 * 
 * This class also handles all the waiting for resources, like selection slots, cash registers and
 * coffee machines. So effectively it plays a role of semaphore.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class CoffeeSystem {

  private volatile int currentlySelecting;
  private CoffeeMachine[] coffeeMachines;
  private CashRegister[] cashRegisters;

  private Object selectionLock = new Object();
  private Object cashRegistersLock = new Object();
  private Object coffeeMachinesLock = new Object();


  /**
   * Constructor
   */
  public CoffeeSystem() {
    currentlySelecting = 0;

    OrdersCounter counter = new OrdersCounter();

    int numOfCoffeeMachines = SimulationConfig.numberOfCoffeeMachines;
    coffeeMachines = new CoffeeMachine[numOfCoffeeMachines];
    for (int i = 0; i < numOfCoffeeMachines; i++) {
      coffeeMachines[i] = new CoffeeMachine(counter);
    }

    int numOfRegisters = SimulationConfig.numberOfPaymentSlots;
    cashRegisters = new CashRegister[numOfRegisters];
    for (int i = 0; i < numOfRegisters; i++) {
      cashRegisters[i] = new CashRegister(counter);
    }
  }

  /**
   * @return coffee kinds available for purchase
   */
  public CoffeeKind[] getAvailableCoffeeKinds() {
    return CoffeeKind.values();
  }

  /**
   * Get a slot (possibility) to see the menu and choose a coffee kind. The system only supports a
   * limited amount of customers that look at the menu at the same time. This amount is specified in
   * SimulationConfig.numberOfSelectionSlots.
   */
  public void getSelectionSlot() {
    while (currentlySelecting >= SimulationConfig.numberOfSelectionSlots) {
      synchronized (selectionLock) {
        try {
          selectionLock.wait();
        } catch (InterruptedException e) {
        }
      }
    }
    currentlySelecting++;
  }

  /**
   * Free up a selection slot, giving the next customer a possibility to see the menu.
   */
  public void freeUpSelectionSlot() {
    currentlySelecting--;
    if (currentlySelecting <= SimulationConfig.numberOfSelectionSlots - 1) {
      synchronized (selectionLock) {
        selectionLock.notify();
      }
    }
  }

  /**
   * Get a slot (possibility) to pay for the chosen coffee. The system only supports a limited
   * amount of customers that pay at the same time. This amount is specified in
   * SimulationConfig.numberOfPaymentSlots.
   */
  public CashRegister getCashRegister() {
    CashRegister result = null;
    while (result == null) {
      for (CashRegister cashRegister : cashRegisters) {
        synchronized (cashRegister) {
          if (cashRegister.isIdle()) {
            result = cashRegister;
            result.setIdle(false);
            break;
          }
        }
      }
      if (result == null) {
        synchronized (cashRegistersLock) {
          try {
            cashRegistersLock.wait();
          } catch (InterruptedException e) {
          }
        }
      }
    }
    return result;
  }

  /**
   * Free up a payment slot, giving the next customer a possibility to pay for their coffee.
   */
  public void freeUpRegisterSlot(CashRegister cashRegister) {
    synchronized (cashRegister) {
      cashRegister.setIdle(true);
    }
    synchronized (cashRegistersLock) {
      cashRegistersLock.notify();
    }
  }

  /**
   * Get a free coffee machine to get paid coffee from. The system only supports a limited amount of
   * coffee machines, that work simultaneously. This amount is specified in
   * SimulationConfig.numberOfCoffeeMachines.
   */
  public CoffeeMachine getCoffeeMachine() {
    CoffeeMachine result = null;
    while (result == null) {
      for (CoffeeMachine coffeeMachine : coffeeMachines) {
        synchronized (coffeeMachine) {
          if (coffeeMachine.isIdle()) {
            result = coffeeMachine;
            result.setIdle(false);
            break;
          }
        }
      }
      if (result == null) {
        synchronized (coffeeMachinesLock) {
          try {
            coffeeMachinesLock.wait();
          } catch (InterruptedException e) {
          }
        }
      }
    }
    return result;
  }



  /**
   * Free up a coffee machine
   * 
   * @param coffeeMachine coffee machine that is being freed up
   */
  public void freeUpCoffeeMachine(CoffeeMachine coffeeMachine) {
    synchronized (coffeeMachine) {
      coffeeMachine.setIdle(true);
    }
    synchronized (coffeeMachinesLock) {
      coffeeMachinesLock.notify();
    }
  }

  /**
   * @return all accepted payment methods
   */
  public PaymentKind[] getAvailablePaymentKinds() {
    return PaymentKind.values();
  }


  /**
   * Provide statistics about how much coffee the coffee machines have poured. The result is a 2D
   * array, where the first index is the index of a coffee machine and the second index represents a
   * coffee type: 1-Espresso, 2-Latte Macchiato, 3-Cappuccino.
   * 
   * @return statistics about how much coffee was dispensed
   */
  public int[][] getDispensedCoffee() {
    int[][] result = new int[coffeeMachines.length][3];
    for (int i = 0; i < coffeeMachines.length; i++) {
      result[i][0] = coffeeMachines[i].getDispensedEspresso();
      result[i][1] = coffeeMachines[i].getDispensedLatteMacchiato();
      result[i][2] = coffeeMachines[i].getDispensedCappuccino();
    }
    return result;
  }

  /**
   * @return statistics about how many times the credit payment was used on all cash registers
   */
  public int getCoffeeSoldForCredit() {
    int result = 0;
    for (CashRegister cashRegister : cashRegisters) {
      result += cashRegister.getCoffeeSoldForCredit();
    }
    return result;
  }

  /**
   * @return statistics about how many times the cash payment was used on all cash registers
   */
  public int getCoffeeSoldForCash() {
    int result = 0;
    for (CashRegister cashRegister : cashRegisters) {
      result += cashRegister.getCoffeeSoldForCash();
    }
    return result;
  }
}
