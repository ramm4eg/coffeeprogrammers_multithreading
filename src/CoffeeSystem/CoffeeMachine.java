
package CoffeeSystem;

import Simulation.SimulationConfig;

/**
 * This class represents a coffee machine that pours selected beverage into a cups
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class CoffeeMachine {
  private volatile boolean idle = true;
  private volatile int dispensedEspresso = 0;
  private volatile int dispensedLatteMacchiato = 0;
  private volatile int dispensedCappuccino = 0;
  private OrdersCounter ordersCounter;

  /**
   * @param counter orders counter
   */
  public CoffeeMachine(OrdersCounter counter) {
    ordersCounter = counter;
  }

  /**
   * Pour coffee of the provided kind. This takes time and increases the corresponding dispensed
   * coffee counter. Before the coffee is dispensed, the check is done, if the coffee has been paid
   * for. If not - no coffee is dispensed.
   * 
   * @param kind coffee kind to dispense
   */
  public void pourCoffee(CoffeeKind kind) {
    boolean orderOk = false;
    synchronized (ordersCounter) {
      orderOk = ordersCounter.dispensing(kind);
    }

    try {
      switch (kind) {
        case Espresso:
          if (orderOk) {
            Thread.sleep(SimulationConfig.ESPRESSO_POURING_TIME);
            dispensedEspresso++;
          }
          break;
        case LatteMacchiato:
          if (orderOk) {
            Thread.sleep(SimulationConfig.LATTE_MACCHIATO_POURING_TIME);
            dispensedLatteMacchiato++;
          }
          break;
        case Cappuccino:
          if (orderOk) {
            Thread.sleep(SimulationConfig.CAPPUCCINO_POURING_TIME);
            dispensedCappuccino++;
          }
          break;
        default:
          // can't happen
          break;
      }
    } catch (InterruptedException e) {
    }
  }

  /**
   * @return true if the machine is idle and can be used by new customer; false otherwise
   */
  public boolean isIdle() {
    return idle;
  }

  /**
   * Set the idle/busy state of the machine. This method is expected to be called by the system upon
   * occupation and freeing of the machine.
   * 
   * @param b true, when freeing up the machine; false, when occupying it
   */
  public void setIdle(boolean b) {
    idle = b;
  }

  /**
   * @return number of espresso cups dispensed
   */
  public int getDispensedEspresso() {
    return dispensedEspresso;
  }

  /**
   * @return number of latte macchiato cups dispensed
   */
  public int getDispensedLatteMacchiato() {
    return dispensedLatteMacchiato;
  }

  /**
   * @return number of cappuccino cups dispensed
   */
  public int getDispensedCappuccino() {
    return dispensedCappuccino;
  }
}
