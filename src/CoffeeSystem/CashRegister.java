
package CoffeeSystem;

import Simulation.SimulationConfig;

/**
 * This class implements a cash register, which gives customers the possibility to pay for their
 * coffee. One customer at a time.
 * 
 * @author Mykhaylo Shlyakhtovskyy
 */
public class CashRegister {

  private volatile boolean idle = true;
  private volatile int coffeeSoldForCredit;
  private volatile int coffeeSoldForCash;
  private OrdersCounter ordersCounter;

  /**
   * Constructor
   * 
   * @param counter orders counter
   */
  public CashRegister(OrdersCounter counter) {
    ordersCounter = counter;
  }

  /**
   * Take payment for the coffee with provided payment kind. This takes time. Additionally, register
   * the paid coffee in the orders counter, so it can later be dispensed by a coffee machine.
   * 
   * @param paymentKind payment kind
   * @param selection coffee kind
   */
  public void takePayment(PaymentKind paymentKind, CoffeeKind selection) {
    try {
      switch (paymentKind) {
        case Credit:
          coffeeSoldForCredit++;
          Thread.sleep(SimulationConfig.CREDIT_PAYMENT_TIME);
          break;
        case Cash:
          coffeeSoldForCash++;
          Thread.sleep(SimulationConfig.CASH_PAYMENT_TIME);
          break;
        default:
          // can't happen
          break;
      }
    } catch (InterruptedException e) {
    }

    synchronized (ordersCounter) {
      switch (selection) {
        case Espresso:
          ordersCounter.espressoOrdered();
          break;
        case LatteMacchiato:
          ordersCounter.latteMacchiatoOrdered();
          break;
        case Cappuccino:
          ordersCounter.cappuccinoOrdered();
          break;
        default:
          // can't happen
          break;
      }
    }
  }

  /**
   * @return statistics about how many times the credit payment was used
   */
  public int getCoffeeSoldForCredit() {
    return coffeeSoldForCredit;
  }

  /**
   * @return statistics about how many times the cash payment was used
   */
  public int getCoffeeSoldForCash() {
    return coffeeSoldForCash;
  }

  /**
   * @return true if the register is idle and can be used by new customer; false otherwise
   */
  public boolean isIdle() {
    return idle;
  }

  /**
   * Set the idle/busy state of the register. This method is expected to be called by the system
   * upon occupation and freeing of the machine.
   * 
   * @param b true, when freeing up the register; false, when occupying it
   */
  public void setIdle(boolean b) {
    idle = b;
  }

}
